 $(function(){

 $('.owl-carousel#sync1').owlCarousel({
        items:1,
        loop:true,
        nav: true,
        navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash'
    });


 $('.owl-carousel#sync2').owlCarousel({
    loop:true,
    margin:1,
    nav:true,
    navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
});

 $('.owl-carousel#sync3').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
    responsive:{
        0:{
            items:1
        },
        400:
        {
            items:2,
            mouseDrag:true
        },
        600:{
            items:3,
            mouseDrag:true
        
        },
        1000:{
            items:4
        }
    }
});

$('.owl-carousel#sync4').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
    responsive:{
        0:{
            items:1
        },
        400:
        {
           items:2,
            mouseDrag:true
        },
        600:{
            items:3
        },
        1000:{
            items:4
            
        }
    }
});


$('.owl-carousel#sync5').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
    responsive:{
        0:{
            items:3
        },
        400:
        {
           items:4,
            mouseDrag:true
        },
        600:{
            items:6
        },
        1000:{
            items:9
            
        }
    }
});

});

      

    
